import React, { useState } from "react";
import { ChakraProvider, Flex, Input, Link, Text } from "@chakra-ui/react";
import { ExternalLinkIcon } from "@chakra-ui/icons";

import "./App.css";
import ten from "./lists/ten.json";
import hundred from "./lists/hundred.json";
import thousand from "./lists/thousand.json";

const getUserMaxAllowance = (address: string): string => {
  if (ten.includes(address)) {
    return "10,000";
  } else if (hundred.includes(address)) {
    return "25,000";
  } else if (thousand.includes(address)) {
    return "125,000";
  }
  return "0";
};

function App(): JSX.Element {
  const [address, setAddress] = useState<string>("");
  const allowance = getUserMaxAllowance(address);
  return (
    <ChakraProvider>
      <Flex
        flexDirection={"column"}
        justifyContent="center"
        alignItems={"center"}
        height="100vh"
        bgGradient="linear(to-t, blue.400, blue.700)"
      >
        <Flex
          w="75%"
          flexDirection={"column"}
          justifyContent="space-between"
          alignItems={"center"}
        >
          <Text color={"white"}>
            Check your whitelist allowance for UMAMI's USDC vault
          </Text>
          <Input
            marginBottom={"1rem"}
            marginTop={"1rem"}
            placeholder="Enter your address"
            value={address}
            onChange={(event) => setAddress(event.target.value)}
            maxWidth="50%"
            textAlign={"center"}
            color={"white"}
            textColor="white"
            textDecorationColor={"white"}
            _placeholder={{ opacity: 1, color: "white" }}
          />
          {address.length === 42 ? (
            parseFloat(allowance) > 0 ? (
              <Text color={"white"}>
                Congrats ! You can deposit up to {allowance} USDC !
              </Text>
            ) : (
              <Text color={"orange"}>
                Sorry, your address doesn't meet the requirements.
              </Text>
            )
          ) : (
            <></>
          )}
          <Link
            marginTop={"1rem"}
            color={"white"}
            href="https://discord.gg/GbNaPeAy"
            isExternal
          >
            Join UMAMI Discord for more informations
            <ExternalLinkIcon mx="2px" />
          </Link>
        </Flex>
      </Flex>
    </ChakraProvider>
  );
}

export default App;
